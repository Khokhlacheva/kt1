import java.util.Arrays;
import java.util.Comparator;

public class Sheep {

   enum Animal {sheep, goat};
   
   public static void main (String[] param) {
      // for debugging
   }
   
   public static void reorder (Animal[] animals) {
      //Counting goats
      int count = 0;
      for (Animal animal: animals) {
         if( animal == Animal.goat){
            count++;
         }
      }
      //Adding goats to the array
      for (int i = 0; i < animals.length; i++) {
         if( i < count){
            animals[i] = Animal.goat;
         }
         //if array if longer than amount of goats sheeps are added
         else {
            animals[i] = Animal.sheep;
         }
      }
   }
}

